import Vue from "vue";
import iView from "view-design";
import locale from "view-design/dist/locale/ja-JP";
import "view-design/dist/styles/iview.css";

Vue.use(iView, { locale });
