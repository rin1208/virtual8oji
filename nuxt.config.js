module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "Virtual8oji",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Virtual8oji" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, ctx) {}
  },

  buildModules: ["@nuxtjs/dotenv"],
  plugins: [{ src: "~/plugins/iview", ssr: false }, "~/plugins/firebase"],
  css: ["iview/dist/styles/iview.css"],
  mode: "spa"
};
